#!/bin/bash

# Build and deploy on master branch
if [[ $CI_COMMIT_REF_SLUG == 'master' ]]; then
    echo "Connecting to docker hub"
    echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USERNAME" --password-stdin

    changed_lines=$(git diff HEAD~1 HEAD docker/Dockerfile.base | wc -l)

    if [ $changed_lines != '0' ]; then
      echo "Dockerfile.base was changed"

      echo "Building site base"
      docker build -t pythondiscord/site-base:latest -f docker/Dockerfile.base .

      echo "Pushing image to Docker Hub"
      docker push pythondiscord/site-base:latest
    else
      echo "Dockerfile.base was not changed, not building"
    fi


    echo "Building image"
    docker build -t pythondiscord/site:latest -f docker/Dockerfile .

    echo "Pushing image"
    docker push pythondiscord/site:latest

    echo "Deploying container"
    curl -H "token: $AUTODEPLOY_TOKEN" $AUTODEPLOY_WEBHOOK
else
    echo "Skipping deploy"
fi
